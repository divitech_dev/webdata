import { Component } from '@angular/core';

@Component({
    selector: 'not-found',
    template: `
        <banner [title]="title" [subtitle]="subtitle"></banner>
    `
})
export class NotFoundComponent {
    title = "wrong:pivot";
    subtitle = "You are not where you thought would be!";
}