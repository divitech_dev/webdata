import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'events-page',
    templateUrl: 'events.component.html'
})
export class EventsComponent implements OnInit {

    benefits: Array<string>;

    constructor() { 
        this.benefits = [
            "Professional questionnaire tailored made for maximum data quality",
            "Quick evaluation of the applications",
            "Comprehensive reports about your attending startups"
        ];
    }

    ngOnInit() { }
}