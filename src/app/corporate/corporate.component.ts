import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'corporate-page',
    templateUrl: 'corporate.component.html'
})
export class CorporateComponent implements OnInit {
    features: Array<string>;

    constructor() {
        this.features = [
            "Dedicated scouting  services - no need to hunt and search for startups",
            "Tailored sourcing services - comprehensive analytics and consolidate reports on qualified startups",
            "Get your own customized DealMatrix platform to suit YOUR NEEDS!"
        ];
     }

    ngOnInit() { }
}