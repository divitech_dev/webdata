import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'contact-page',
    templateUrl: 'contact.component.html',
    styles: [
        `
            b, span {
                display: block;
            }

            b {
                margin-top: 20px;
            }
        `
    ]
})
export class ContactComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}