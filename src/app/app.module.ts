import { BrowserModule } from '@angular/platform-browser';
import {APP_BASE_HREF} from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule, routableComponents } from './app-routing.module';
import { ApiService } from './services';

import {
  FooterComponent,
  HeaderComponent,
  BannerComponent,
  ContentTextComponent,
  TeamComponent,
  CompanyComponent,
  TeamMemeberComponent,
  ContactFormComponent
} from './shared';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],   
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    BannerComponent,
    ContentTextComponent,
    TeamComponent,
    CompanyComponent,
    TeamMemeberComponent,
    ContactFormComponent,
    routableComponents
  ],
  providers: [
    ApiService,
    {provide: APP_BASE_HREF, useValue : '/' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
