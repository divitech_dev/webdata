export class TeamMember {
    constructor (
        public name: String,
        public image: String,
        public position: String,
        public facebook?: String,
        public linkedin?: String
    ) {}
}