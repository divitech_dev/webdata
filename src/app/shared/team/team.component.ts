import { Component, OnInit, Input } from '@angular/core';
import { TeamMember } from '../models/team-member.model';
import { TeamService } from '../../services';

@Component({
    selector: 'team',
    templateUrl: 'team.component.html',
    providers: [ 
        TeamService
    ],
    styles: [`
        .eqWrap {
            display: flex;
        }

        .eq {
            padding: 10px;
        }

        .equalHMRWrap {
            flex: 1;            
            flex-wrap: wrap;
        }
    `]
})

export class TeamComponent implements OnInit {
    members: [TeamMember];

    constructor(
        private teamService: TeamService
    ) { }

    ngOnInit() { 
        this.teamService.getTeam()
        .subscribe(members => {
            this.members = members;
        });
    }
}