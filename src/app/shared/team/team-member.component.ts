import { Component, OnInit, Input } from '@angular/core';
import { TeamMember } from '../models'

@Component({
    selector: 'team-member',
    templateUrl: 'team-member.component.html',
    styles: [`
        div {
            text-align: center;
            margin-bottom: 20px;
        }
        span {
            display: block;
        }

        span:first-of-type {
            font-weight: bold;
            font-size: bigger;
        }

        a:last-of-type {
            margin-right: 0;
        }

        a {
            margin-right: 20px;
        }

        .eq {
            padding: 10px;
        }

        .equalHMR {
            width: 180px;
            margin-bottom: 2%;
        }  

        img {
            height: 150px; 
        }      

    `]
})
export class TeamMemeberComponent implements OnInit {
    @Input() member: TeamMember;

    constructor() { }

    ngOnInit() { }
}