import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'banner',
    templateUrl: 'banner.component.html',
    styles: [
        `
            header {
                background-color: #000
            }
        `
    ]
})
export class BannerComponent implements OnInit {
    @Input() title: string;
    @Input() subtitle: string;

    constructor() { }

    ngOnInit() { }
}