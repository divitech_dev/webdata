import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { fakeAsync, async, inject, tick, TestBed, getTestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HeaderComponent } from './header.component';
import { AppModule } from '../../app.module';
import { routes } from '../../app-routing.module';

describe('Router tests', () => {
  //setup
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        AppModule
      ]
    });
  });
  
  //specs
  it('can navigate to startups (async)', async(() => {
    let fixture = TestBed.createComponent(HeaderComponent);
    TestBed.get(Router)
      .navigate(['/startups'])
        .then(() => {
          expect(location.pathname.endsWith('/startups')).toBe(true);
        }).catch(e => console.log(e));
  }));
  
  it('can navigate to home (async)', async(() => {
    let fixture = TestBed.createComponent(HeaderComponent);
    TestBed.get(Router)
      .navigate(['/home'])
        .then(() => {
          expect(location.pathname.endsWith('/home')).toBe(true);
        }).catch(e => console.log(e));
  }));  
});
