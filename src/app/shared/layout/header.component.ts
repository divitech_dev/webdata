import { Component, } from '@angular/core';


@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  menuIsClicked : boolean = false;

  constructor() {}

  onMenuClick() {
    this.menuIsClicked = !this.menuIsClicked;
  }

  onItemsClick() {
    this.menuIsClicked = false;
  }
}
