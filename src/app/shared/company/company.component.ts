import { Component, OnInit, Input } from '@angular/core';
import { Company } from '../models';
import { CompanyService } from '../../services/company.service';

@Component({
    selector: 'companies',
    templateUrl: 'company.component.html',
    providers: [ 
        CompanyService
    ],
    styles: [
        `
            img {
                width: 150px;
                max-height: 150px;
                padding: 20px;
            }
        `
    ]
})

export class CompanyComponent implements OnInit {
    companies: [Company];

    constructor(
        private companyService: CompanyService
    ) { }

    ngOnInit() { 
        this.companyService.getCompanies()
        .subscribe(companies => {
            this.companies = companies;
        });
    }
}