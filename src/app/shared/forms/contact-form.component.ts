import { Component, OnInit, ViewChild } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/operator/filter';
import { FormsService } from '../../services';

@Component({
    selector: 'contact-form',
    templateUrl: 'contact-form.component.html',
    styles: [`
      form.ng-valid  {

      }
    `],
    providers: [ FormsService ]
})
export class ContactFormComponent {
    @ViewChild('formRef') form;
    msg: string = '';
    postMsg: string;

    constructor(
      private formsService: FormsService
    ) {}

    onSubmit(formValue) {
      this.formsService.save(formValue).subscribe(postMsg => {
        this.postMsg = postMsg;
      });
    }   
}