import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { Company } from '../shared';

@Injectable()
export class CompanyService {
  constructor (
    private apiService: ApiService
  ) {}

  getCompanies(): Observable<[Company]> {
    return this.apiService.get('/companies')
           .map(data => data.companies);
  }
}
