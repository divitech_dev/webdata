import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';

@Injectable()
export class FormsService {

    constructor (
        private apiService: ApiService
    ) {}

    save(form): Observable<string> {
        return this.apiService.post('contact', {form: form})
                        .map(data => data.message);
    }
}