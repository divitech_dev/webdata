import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { TeamMember } from '../shared';

@Injectable()
export class TeamService {
  constructor (
    private apiService: ApiService
  ) {}

  getTeam(): Observable<[TeamMember]> {
    return this.apiService.get('/team')
           .map(data => data.team);
  }
}
