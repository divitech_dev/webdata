import {
  TestBed,
  getTestBed,
  async,
  inject
} from '@angular/core/testing';
import {
  Headers, BaseRequestOptions,
  Response, HttpModule, Http, XHRBackend, RequestMethod
} from '@angular/http';

import {ResponseOptions} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {TeamMember} from '../shared';
import {ApiService} from './api.service';
import { environment } from '../../environments/environment';

describe('API Service', () => {
  let mockBackend: MockBackend;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        ApiService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          deps: [MockBackend, BaseRequestOptions],
          useFactory:
            (backend: XHRBackend, defaultOptions: BaseRequestOptions) => {
              return new Http(backend, defaultOptions);
            }
       }
      ],
      imports: [
        HttpModule
      ]
    });
    mockBackend = getTestBed().get(MockBackend);
  }));

  it('should make a GET request', done => {
    let apiService: ApiService;

    getTestBed().compileComponents().then(() => {
      mockBackend.connections.subscribe(
        (connection: MockConnection) => {
          connection.mockRespond(new Response(
            new ResponseOptions({
                body: [{
                        photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/1478007442.png",
                        name: "Christoph Drescher",
                        position: "Co-Founder & CEO",
                        facebook: "https://www.facebook.com/christoph.drescher.77?fref=ts",
                        linkedin: "https://www.linkedin.com/in/christophdrescher"
                  }]
              }
            )));
        });

        apiService = getTestBed().get(ApiService);
        expect(apiService).toBeDefined();

        apiService.get('uri').subscribe((data: TeamMember[]) => {
            expect(data.length).toBeDefined();
            expect(data.length).toEqual(1);
            expect(data[0].name).toBe("Christoph Drescher");
            done();
        });
    });
  });

  it('should make a GET request async',
    async(inject([ApiService], (apiService) => {
      mockBackend.connections.subscribe(
        (connection: MockConnection) => {
          connection.mockRespond(new Response(
            new ResponseOptions({
                body: [
                        "https://dealscreening.net/uploads/9/1/0/4/91048542/rzb-group.png?125",
                        "https://dealscreening.net/uploads/9/1/0/4/91048542/bitsandpretzels-main-image.png?60",
                        "https://dealscreening.net/uploads/9/1/0/4/91048542/screen-shot-2016-11-04-at-12-08-04-am_orig.png",
                    ]
              }
            )));
        });

      apiService.get('uri').subscribe(
        (data) => {
            expect(data.length).toBeDefined();
            expect(data.length).toEqual(3);
            expect(data[0]).toEqual("https://dealscreening.net/uploads/9/1/0/4/91048542/rzb-group.png?125");
      });
    })));

  it('should make a POST request',
    async(inject([ApiService], (apiService) => {
      mockBackend.connections.subscribe((connection: MockConnection) => {
        expect(connection.request.url).toBe(environment.apiURL + 'testuri');
        expect(connection.request.method).toBe(RequestMethod.Post);
        connection.mockRespond(new Response(<any>{body: '{"response": "OK"}'}));
      });

      let data = { name: 'Test' };
      apiService.post('testuri', data).subscribe(
        (successResult) => {
          expect(successResult).toBeDefined();
          expect(successResult).toEqual({'response': 'OK'});
        });
    })));    

});