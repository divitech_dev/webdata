import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { CorporateComponent } from './corporate/corporate.component';
import { EventsComponent } from './events/events.component';
import { InvestorsComponent } from './investors/investors.component';
import { StartupsComponent } from './startups/startups.component';
import { NotFoundComponent } from './not-found.component';


export const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent, },
  { path: 'contact', component: ContactComponent },
  { path: 'corporate', component: CorporateComponent },
  { path: 'events', component: EventsComponent },
  { path: 'investors', component: InvestorsComponent },
  { path: 'startups', component: StartupsComponent },
  { path: '**', pathMatch: 'full', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routableComponents = [
  HomeComponent,
  ContactComponent,
  CorporateComponent,
  EventsComponent,
  InvestorsComponent,
  StartupsComponent,
  NotFoundComponent
];