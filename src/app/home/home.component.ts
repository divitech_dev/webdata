import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'home-page',
    templateUrl: 'home.component.html',
    styles: [
        `
            ul {
                padding: 0;
                margin: 0;
                list-style-type: none;
            }
            img.product {
                width:350px;
                padding-top: 20px;
            }
            .right {
                float: right;
            }
        `
    ]
})
export class HomeComponent implements OnInit {
    features: string[] = [
        "Elegant jury scoring solution",
        "Involve the pitch session audience in rating the startups",
        "Instant results of pitch competitions with live statistics",
        "Complete reports on all the startups rating activities",
        "Professional statistics ready for interpretation",
    ]

    screenLiteFeatures: string[] = [
        "Tailored screening for project based screening",
        "Fully customizable questionnaire",
        "Beautifully designed data input forms for your startups",
        ​"Dedicated landing page for your project",
        "Comprehensive reports of all your applications",
        "Formated excel reports of data points"
    ];

    screenFeatures: string[] = [
        "Optimize and structure your screening process",
        "Venture specific questions set",
        "Intuitive dashboard",
        "Enhanced data interpretation",
        "Score and compare your startups applications",
        "Startup report with PDF export​​"
    ];

    constructor() { }

    ngOnInit() { }
}