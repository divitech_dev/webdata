module.exports = {
    team: [
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/1478007442.png",
            name: "Christoph Drescher",
            position: "Co-Founder & CEO",
            facebook: "https://www.facebook.com/christoph.drescher.77?fref=ts",
            linkedin: "https://www.linkedin.com/in/christophdrescher"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/michai-coman-dealmatrix-coo_orig.png",
            name: "Mihai Coman",
            position: "Co-Founder & CEO",
            facebook: "https://www.facebook.com/chris.houghtaling?fref=ts",
            linkedin: "https://at.linkedin.com/in/mhcoman"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/michael-ferjancic-dealmatrix-cto_orig.png",
            name: "Michael Ferjancic",
            position: "Co-Founder & CEO",
            facebook: "",
            linkedin: "https://at.linkedin.com/in/michael-ferjancic-6925811b/en"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/chris-hougthaling-portra-t.png?181",
            name: "Chris Houghtaling",
            position: "CMO",
            facebook: "https://www.facebook.com/chris.houghtaling?fref=ts",
            linkedin: "https://at.linkedin.com/in/chrishoughtaling"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/lena-traninger-dealmatrix-business-development_orig.png",
            name: "Lena Traninger",
            position: "Business and Product Development",
            facebook: "https://www.facebook.com/lena.susanna.t?fref=ts",
            linkedin: "https://at.linkedin.com/in/lenatraninger/en"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/aaeaaqaaaaaaaaioaaaajgqymgi5mzmzlwy2mzytnduxys05zjfjltyxyjk1yjk3zmq3yg.png?173",
            name: "Elisabeth Houghtaling",
            position: "Marketing & Communications Specialist",
            facebook: "https://www.facebook.com/elisabeth.houghtaling?fref=ts",
            linkedin: "https://at.linkedin.com/in/elisabeth-houghtaling-a009063"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/mohamed-kassem-dealmatrix-business-developer.png?193",
            name: "Mohamed Kassem",
            position: "Business Development",
            facebook: "https://www.facebook.com/mohamedkassem5?fref=ts",
            linkedin: "https://at.linkedin.com/in/mokassem"
        },
        {
            photo: "https://dealscreening.net/uploads/9/1/0/4/91048542/natalie-korotaeva-dealmatrix-designer.png?201",
            name: "Natalie Korotaeva",
            position: "UX/UI Designer",
            facebook: "https://www.facebook.com/korotaeva.natalie?fref=ts",
            linkedin: "https://at.linkedin.com/in/korotaeva/en"
        }
    ]
}