var mongoose = require('mongoose');

var ContactSchema = new mongoose.Schema({
  name: String,
  lastname: String,
  email: String,
  description: String
}, {timestamps: true});

mongoose.model('Contact', ContactSchema);
