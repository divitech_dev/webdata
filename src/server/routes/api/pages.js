var mongoose = require('mongoose');
var router = require('express').Router();
var team = require('../../mocks/team.js');
var companies = require('../../mocks/companies.js');

router.get('/team', function(req, res, next){
    return res.json(team);
});

router.get('/companies', function(req, res, next){
    return res.json(companies);
});

module.exports = router;