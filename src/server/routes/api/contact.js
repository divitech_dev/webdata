var mongoose = require('mongoose');
var router = require('express').Router();
var Contact = mongoose.model('Contact');

router.post('/', function(req, res, next){
  var form = new Contact();

  form.name = req.body.form.name;
  form.lastname = req.body.form.lastname;
  form.email = req.body.form.email;
  form.description = req.body.form.description;

  form.save().then(function(){
    return res.json({
        status: 200,
        message: "The form is successfully saved!"
    });
  }).catch(next);
});

module.exports = router;