# Dealmatrix

The Angular project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.21.

Run `node src/server/server.js` to start the server and `ng serve` to compile the project. 

The server runs by default on port 3000 while the angular project on port 4200.

Run `ng test` to run the tests. The test coverage is partial to ApiService and HeaderComponent.