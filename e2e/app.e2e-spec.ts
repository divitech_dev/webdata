import { DealmatrixPage } from './app.po';

describe('dealmatrix App', function() {
  let page: DealmatrixPage;

  beforeEach(() => {
    page = new DealmatrixPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
